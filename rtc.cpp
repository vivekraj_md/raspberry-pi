#include<iostream>
#include<fstream>
#include<fcntl.h>
#include<sys/ioctl.h>
#include<linux/i2c.h>
#include<linux/i2c-dev.h>
#include<unistd.h>
#include<string>
#include<ctime>
#include<linux/rtc.h>

#define BUFFER_SIZE 26      //0x00 to 0x19
#define LED0_PATH "/sys/class/leds/led0"
using namespace std;

class Alarm_rtc{
	protected: 
		int file;
		char buf[BUFFER_SIZE];
		char rbuf[BUFFER_SIZE];
		char wbuff[BUFFER_SIZE];
		char rbuff[BUFFER_SIZE];
		tm al_t;
		int msec;
		char wBuf[BUFFER_SIZE];
		fstream fs;
		char wBuffer[BUFFER_SIZE];

	public:
		char bcdToDec(char digit);
		char decToBcd(int dig);
		char reset_add(char writeBuffer );
		char buf_size(char buf);
		char read_buf();
		void sensor_ctl();
		char write_buf();
		string day();
		Alarm_rtc (int);
		char set_alarm();
	 	char set_current();
		char trigger_alarm();
		void rem_trigger();
		char sq_wave();
		char feature_clk();
};

Alarm_rtc::Alarm_rtc(int file):file(file){
	cout<<" [The DS1307 has been started] "<< endl;
	cout<<" The file has been opened"<< endl;
	this->sensor_ctl();  //Build connection to the sensor	 
	this->write_buf();   //pointing the register to write
	this->read_buf();    //Read the time from the registers
	printf(" The time of RTC Module :- %02d:%02d:%02d\n", bcdToDec(buf[2]), bcdToDec(buf[1]), bcdToDec(buf[0]));
	printf(" Date of displayed time of RTC :- %02d:%02d:%02d\n",bcdToDec(buf[4]), bcdToDec(buf[5]), bcdToDec(buf[6]));  
	cout<<" The day of week :- "<<day()<<endl;
	this->set_current(); //Current date and time written and read
	this->set_alarm();   //Alarm time i set 
	this->trigger_alarm();  //Trigger the alarm when it matches the actual current time
	this->sq_wave();        //Square wave functionality 
	this->feature_clk();    //Feature - Displays part of the day
	close(file);
}

char Alarm_rtc::bcdToDec(char digit){
	int btc = (digit/16)*10+(digit%16);
	return btc;
	}

char Alarm_rtc::read_buf(){ 	    
	if(file < 0){
	perror("failed to open the bus\n");}
	if(read(file,buf, BUFFER_SIZE)!=BUFFER_SIZE){
	perror("Failed to read in the buffer\n");
	}
}
void Alarm_rtc::sensor_ctl(){
	if(ioctl(file, I2C_SLAVE, 0x68) < 0){
        perror("Failed to connect to the sensor\n");
  	};
}
char Alarm_rtc::write_buf(){
	char writeBuffer[1] = {0x00};
	if(write(file, writeBuffer, 1)!=1){
	perror("Failed to reset the read address\n");
	}
}

string Alarm_rtc::day(){
		
	int d = bcdToDec(buf[3]);
	switch (d){
	case 1:
		return "Sunday";
	case 2:
		return "Monday";
	case 3:
		return "Tuesday";
	case 4:
		return "Wednesday";
	case 5:
		return "Thursday";
	case 6:
		return "Friday";
	case 7:
		return "Saturday";
	default:
		return "error";
	}
}

 char Alarm_rtc::decToBcd(int dig){
	char dec = (dig/10)*16+(dig%10);
	return dec;
	}


char Alarm_rtc::set_current(){
	time_t ti_me= time(0);
	tm *loc_time=localtime(&ti_me); //Local time
	char wbuff[7] = {0x14,decToBcd( loc_time->tm_sec),decToBcd( loc_time->tm_min),decToBcd(loc_time->tm_hour),
		decToBcd( loc_time->tm_mday),decToBcd (loc_time->tm_mon+1),decToBcd(loc_time->tm_year+1900)};
	if(write(file, wbuff, 7)!=7){
	perror("Failed to reset the read address\n");
	}	
	if(write(file, wbuff, 1)!=1){    //From register 0x14
	perror("Failed to reset the read address\n");
	}

	char rbuff[BUFFER_SIZE];
	if(read(file, rbuff, BUFFER_SIZE)!=BUFFER_SIZE){
	perror("Failed to read in the buffer\n");
}
	printf (" The alarm is set to the date %02d-%02d-%04d and time :- %02d:%02d:%2d\n",
	loc_time->tm_mday,
	loc_time->tm_mon+1,
	loc_time->tm_year+1900,
	loc_time->tm_hour,
	loc_time->tm_min,
	loc_time->tm_sec);
	return 0;
	}

char Alarm_rtc::set_alarm(){
	time_t s;
	time_t m;
	time_t al=time(0);
	al_t =*localtime(&al);
	cout<<" Enter the seconds to set an alarm \t"<<endl;
	cin>> s ;      //Enter value in seconds to trigger the alarm 
	cout<<" seconds"<<endl; 
							
	if (s<0){
	cout<<" Enter the non negative number and try again \t"<< endl;
	return 0;
	}	
	if(s>0){
	al_t.tm_sec= al_t.tm_sec + s;
	m = al_t.tm_sec / 60;
	al_t.tm_min = al_t.tm_min + m;
	al_t.tm_sec =al_t.tm_sec % 60;
	}														
	else if (al_t.tm_min >= 60)
	{
	al_t.tm_hour = al_t.tm_hour + (al_t.tm_min / 60);
	al_t.tm_min =al_t.tm_min %  60;
	}
	else {
	printf(" Error ");
	}
		printf (" The alarm is set to the date %02d-%02d-%04d and time :- %02d:%02d:%2d\n",
		al_t.tm_mday,
		al_t.tm_mon+1,
		al_t.tm_year+1900,
		al_t.tm_hour,al_t.tm_min,al_t.tm_sec);
		return 0;
	}

void Alarm_rtc::rem_trigger(){
	// Remove the trigger from LED
	std::fstream fs;
	fs.open( LED0_PATH "/trigger", std::fstream::out);
	fs << "none";
	fs.close();
	}

char Alarm_rtc::trigger_alarm(){
	char wBuf[4] = {0x08,(char)al_t.tm_sec,(char) al_t.tm_min,(char) al_t.tm_hour};
	if(write(file, wBuf, 4)!=4){
	perror("Failed to reset the read address\n");
	}
	if(write(file, wBuf, 1)!=1){    // From the register 0x08
	perror("Failed to reset the read address\n");
	}
	char rbuf[BUFFER_SIZE];
	if(read(file, rbuf, BUFFER_SIZE)!=BUFFER_SIZE){
	perror("Failed to read in the buffer\n");
	}
	printf(" Alarm time :- %02d:%02d:%02d\n", rbuf[2], rbuf[1],rbuf[0]);

 	for(int i=0; i<1000; i++)
 	{ 
	this->write_buf();
	this->read_buf(); 			// Read the time for the register to perform the comparision with the alarm set time
	if(bcdToDec(buf[0]) == rbuf[0])  	// Seconds comparision
	{
		if(bcdToDec(buf[1]) == rbuf[1]) //Minutes comparision
		{
			if(bcdToDec(buf[2]) == rbuf[2]) //Hours comparision
			{
  				 cout << " Alarm time matched and LED triggered " << endl;
				 rem_trigger();
				 fs.open (LED0_PATH "/brightness", std::fstream::out);
				 fs<< "1";
			         fs.close();
			 	 rem_trigger();
				 usleep(2000000);
				 fs.open (LED0_PATH "/brightness", std::fstream::out);
				 fs << "0";	// switch off the LED
			         fs.close();
				 cout<<" Success"<<endl;
				 break ;
			}
		}	
	}
	usleep(1000000); // Delay for one second after every loop
	}
	return 0;
	}

char Alarm_rtc::sq_wave(){
	char num = 91; // Value of 0b10010001 
	char dtb = (num/10)*16+(num%10);
	cout<<dtb<<endl;
	char wBuffer[2] = {0x07,dtb};
	if(write(file, wBuffer, 2)!=2){
	perror("Failed to reset the read address\n");
	}
	this->read_buf();
	printf(" %02d \n",bcdToDec(buf[0]));
}


char Alarm_rtc::feature_clk(){  //Feature - Describes the part of the day
	this->write_buf();
	this->read_buf();
	if((bcdToDec(buf[2])>= 5) && (bcdToDec(buf[2])<= 8))
	{
	cout<<" Part of the Day: Early Morning "<<endl;
	}
	if((bcdToDec(buf[2])>= 9) && (bcdToDec(buf[2])<= 11))
	{
	cout<<" Part of the Day: Morning "<<endl;
	}
	if((bcdToDec(buf[2])>= 12) && (bcdToDec(buf[2])<= 14))
	{
	cout<<" Part of the Day: Early Afternoon "<<endl;
	}
	if((bcdToDec(buf[2])>= 15) && (bcdToDec(buf[2])<= 16))
	{
	cout<<" Part of the Day: Late Afternoon "<<endl;
	}
	if((bcdToDec(buf[2])>= 17) && (bcdToDec(buf[2])<= 18))
	{
	cout<<" Part of the Day: Early Evening "<<endl;
	}
	if((bcdToDec(buf[2])>= 19) && (bcdToDec(buf[2])<= 20))
	{
	cout<<" Part of the Day: Late evening "<<endl;
	}
	if((bcdToDec(buf[2])>= 21) && (bcdToDec(buf[2])<= 4))
	{
	cout<<" Part of the Day: Night "<<endl;
	}
}

int main()
{
	int file=open("/dev/i2c-1", O_RDWR);
	Alarm_rtc alarm=Alarm_rtc(file);
	return 0;
}


