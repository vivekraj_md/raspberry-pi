#include<iostream>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<sys/time.h>
#include<stdint.h>
#include<sys/ioctl.h>
#include<linux/i2c.h>
#define _ONION_I2C_H_
#define I2C_DEV_PATH "/dev/i2c-%d"
#define DS1307_I2C_ADDRESS 0x68
using namespace std;


// the time is in the registers in encoded decimal form
 int bcdToDec(char b) { return (b/16)*10 + (b%16); }

 int main(){
     int file;
     cout <<"Starting the DS3231 test application" <<endl;
     
     if((file=open("/dev/i2c-2", O_RDWR)) < 0){
     perror("failed to open the bus\n");
     return 1;}
     
     if(ioctl(file, I2C_SLAVE, 0x68) < 0){
     perror("Failed to connect to the sensor\n");
     return 1;}

     char writeBuffer[1] = {0x00};
     if(write(file, writeBuffer, 1)!=1){
     perror("Failed to reset the read address\n");
     return 1;}
     
     char buf[BUFFER_SIZE];
     if(read(file, buf, BUFFER_SIZE)!=BUFFER_SIZE){
     perror("Failed to read in the buffer\n");
     return 1; }

     printf("The RTC time is %02d:%02d:%02d\n", bcdToDec(buf[2]),
     bcdToDec(buf[1]), bcdToDec(buf[0]));
     close(file);
      return 0;}





















/*
class Alarm_time{
	 public:
	        int sec =0;
		int  min, hour, doweek,domonth, mon, year;
 
 };

//bcd to dec converting
int bcdToDec(int value)
{
	return(((value/16)*10) + (value%16));
}

void i2cRead(int sec,int min,int hour,int doweek,int domonth,int mon,int year)
{
i2c.beginTransmission(DS1307_I2C_ADDRESS);
i2c.write(0); //pointer set to 00h
i2c.endTransmission();

//starting from the register 00h

i2c.requestFrom(DS1307_I2C_ADDRESS, 7);
sec = bcdToDec(i2c.read() & 0x7f);
min = bcdToDec(i2c.read());
hour = bcdToDec(i2c.read());
doweek = bcdToDec(i2c.read());
domonth = bcdToDec(i2c.read());
mon = bcdToDec(i2c.read());
year = bcdToDec(i2c.read());
}

*/

 
